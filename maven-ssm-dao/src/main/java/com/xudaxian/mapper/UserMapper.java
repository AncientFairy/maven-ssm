package com.xudaxian.mapper;

import com.xudaxian.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-14 08:40
 */
@Mapper
public interface UserMapper {

    /**
     * 添加用户信息
     *
     * @param user 用户信息对象
     * @return 如果新增成功，则返回true；否则，返回false
     */
    boolean save( User user);

    /**
     * 修改用户信息
     *
     * @param user 用户信息对象
     * @return 如果修改成功，则返回true；否则，返回false
     */
    boolean update( User user);

    /**
     * 删除用户信息
     *
     * @param uuid 主键
     * @return 如果删除成功，则返回true；否则，返回false
     */
    boolean delete(String uuid);

    /**
     * 根据主键查询用户信息
     *
     * @param uuid 主键
     * @return 用户信息
     */
    User get(String uuid);

    /**
     * 查询全部用户信息
     *
     * @return 全部用户信息
     */
    List<User> getAll();

    /**
     * 根据用户名和密码查询用户信息
     *
     * @param username 用户名
     * @param password 密码
     * @return 用户信息
     */
    User getByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

}
