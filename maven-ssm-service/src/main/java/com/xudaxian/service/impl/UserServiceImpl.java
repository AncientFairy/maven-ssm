package com.xudaxian.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xudaxian.domain.User;
import com.xudaxian.mapper.UserMapper;
import com.xudaxian.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-14 10:48
 */
@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean save(User user) {
        return userMapper.save(user);
    }

    @Override
    public boolean update(User user) {
        return userMapper.update(user);
    }

    @Override
    public boolean delete(String uuid) {
        return userMapper.delete(uuid);
    }

    @Override
    public User get(String uuid) {
        return userMapper.get(uuid);
    }

    @Override
    public PageInfo<User> getAll(int page, int size) {
        PageHelper.startPage(page, size);
        List<User> userList = userMapper.getAll();
        return new PageInfo<>(userList);
    }

    @Override
    public User login(String userName, String password) {
        return userMapper.getByUsernameAndPassword(userName, password);
    }
}
