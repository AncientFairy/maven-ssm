package com.xudaxian.service;

import com.xudaxian.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.UUID;

/**
 * @author 许大仙
 * @version 1.0
 * @since 2021-06-14 12:24
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-service.xml" })
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void test(){
        User user = new User();
        user.setUuid(UUID.randomUUID().toString());
        user.setUsername("许大仙");
        user.setPassword("123456");
        user.setRealName("许仙");
        user.setGender(0);
        user.setBirthday(new Date());

        userService.save(user);
    }
}
